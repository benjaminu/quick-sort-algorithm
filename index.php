<?php include_once("config.php");?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>kidivoucher stats</title>
        <style>
            .heading{
                font-weight: bold;
                padding: 2px;
                border: solid 1px #000000;
            }
            .value{
                padding: 2px;
                border-top: solid 1px #000000;
                border-left: none;
                border-bottom: solid 1px #000000;
                border-right: solid 1px #000000;
            }
        </style>
    </head>
    <body>
        <?php
        $data_statistics = new DataAnalyzer("Task1-DataForMeanMedianMode.csv");
        $data_statistics->analyzeData();
        ?>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td class="heading">Total value of payments:</td>
                <td class="value"><?php echo $data_statistics->getPaymentValue();?></td>
            </tr>
            <tr>
                <td class="heading">Average (mean) value of payments:</td>
                <td class="value"><?php echo $data_statistics->getMean();?></td>
            </tr>
            <tr>
                <td class="heading">Payment values that occur most often:</td>
                <td class="value"><?php echo implode(",", $data_statistics->getMode());?></td>
            </tr>
            <tr>
                <td class="heading">These payment values occur:</td>
                <td class="value"><?php echo $data_statistics->getMaxPaymentOccurence();?> times</td>
            </tr>
            <tr>
                <td class="heading">Median value of payments:</td>
                <td class="value"><?php echo $data_statistics->getMedian();?></td>
            </tr>
        </table>
    </body>
</html>
