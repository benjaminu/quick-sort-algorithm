<?php
/**
 * Just a bunch of Helper functions
 *
 * @author Benjamin Ugbene
 */
class Helper
{
    /**
    * @desc confirms if value is even
    * @access public
    * @param long $value
    * @return boolean
    */
    static public function isEven($value)
    {
        return (($value % 2) == 0);
    }//static public function isEven($value)
    
    /**
    * @desc confirms if value is odd
    * @access public
    * @param long $value
    * @return boolean
    */
    static public function isOdd($value)
    {
        return !Helper::isEven($value);
    }//static public function isOdd($value)
}//class Helper
?>
