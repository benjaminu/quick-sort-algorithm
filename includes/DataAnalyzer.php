<?php                         
/**
 * Analyzes data in a csv file, to return statistical results (mean, median, mode)
 *
 * @author beeXtreem
 */
class DataAnalyzer
{
    /**
     * Path to CSV file
     * @access private
     * @var string
     */
    private $filepath;

    /**
     * Average (mean) value of payments
     * @access private
     * @var double
     */
    private $mean;

    /**
     * Payment that occurs most often
     * @access private
     * @var array
     */
    private $mode = array();

    /**
     * Mean value of payments
     * @access private
     * @var double
     */
    private $median;

    /**
     * Total value of payments
     * @access private
     * @var double
     */
    private $payment_total = 0.0;

    /**
     * Number of Payments
     * @access private
     * @var long
     */
    private $num_of_payments = 0;

    /**
     * Array holding payment values and the number of times they occur
     * keys are payment values
     * @access private
     * @var array
     */
    private $payment_occurences = array();

    /**
     * Maximum occurence of Payment Value
     * @access private
     * @var array
     */
    private $max_payment_occurence;

    /**
     * Maximum occurence of Payment Value
     * @access private
     * @var array
     */
    private $payment_values = array();

    /**
    * @desc DataAnalyzer Constructor
    * @access public
    * @param string $filepath
    * @return void 
    */
    public function DataAnalyzer($filepath)
    {
        $this->filepath = $filepath;
    }//public function DataAnalyzer($filepath)
    
    /**
    * @desc Executes Data analysis
    * @access public
    * @param void 
    * @return void 
    */
    public function analyzeData()
    {
        //Open csv file for reading
        if (($filehandle = fopen($this->filepath, "r")) !== FALSE)
        {
            $title_row = fgetcsv($filehandle, 1024, ","); // retrieve the title row

            $count = 0;
            $sum = 0;
			
            //loop through csv line by line
            while (($data = fgetcsv($filehandle, 1024, ",")) !== FALSE)
            {
                $count++;
                
                //payment data is in second column
                $payment_value = $data[1]; 
                $sum += (double)$payment_value; //sum payments
                $this->updatePaymentOcurrence($payment_value); //collate occurence of payments
                $this->payment_values[] = $payment_value; //collate payments
            }//while (($data = fgetcsv($filehandle, 1024, ",")) !== FALSE)
            
            fclose($filehandle); //close csv file

            $this->num_of_payments = $count;
            $this->payment_total = $sum;

            $this->calculateMean();
            $this->calculateMode();

            //use modal payment value as pivot to speed up sorting
            $this->payment_values = Sorter::quickSortArrayAsc($this->payment_values, $this->mode[0]);
            $this->calculateMedian();
        }//if (($handle = fopen($this->filepath, "r")) !== FALSE)
    }//public function DataAnalyzer($filepath)

    /**
    * @desc Updates the Payment Occurence counter
    * @access private
    * @param string $payment_value
    * @return void
    */
    private function updatePaymentOcurrence($payment_value)
    {
        if(isset($this->payment_occurences[$payment_value]))
        {
            $this->payment_occurences[$payment_value]++;
        }//if(isset($this->payment_occurences[$payment_value]))
        else
        {
            $this->payment_occurences[$payment_value] = 1;
        }//end else //if(isset($this->payment_occurences[$payment_value]))
    }//private function updatePaymentOcurrence($payment_value)


    /**
    * @desc Calculates Mean Payment
    * @access private
    * @param void
    * @return void
    */
    private function calculateMean()
    {
        $this->mean = $this->payment_total / $this->num_of_payments;
    }//private function calculateMean()


    /**
    * @desc Calculates Modal Payment
    * @access private
    * @param void
    * @return void
    */
    private function calculateMode()
    {
        $payment_values = array_keys($this->payment_occurences);
        $maxOccurence = $this->payment_occurences[$payment_values[0]];
        $maxPayment = array();
        
        foreach($this->payment_occurences as $payment_value => $occurence)
        {
            if($occurence > $maxOccurence)
            {
                $maxOccurence = $occurence; //add new max value
                
                //store actual payment value
                $maxPayment = array(); //clear all entries
                $maxPayment[0] = $payment_value;
            }//if($occurence > $maxOccurence)
            elseif($occurence == $maxOccurence)
            {
                //add corresponding max payment value(s)
                $maxPayment[] = $payment_value; 
            }//elseif($occurence == $maxOccurence)
        }//foreach($this->payment_occurences as $payment_value => $occurence)
        
        $this->mode = $maxPayment;
        $this->max_payment_occurence = $maxOccurence;
    }//private function calculateMode()

    /**
    * @desc Calculates Median Payment
    * @access private
    * @param void
    * @return void
    */
    private function calculateMedian()
    {
        $size = count($this->payment_values);

        if(Helper::isOdd($size))
        {
            $index = ($size + 1)/2; //middle value
            $this->median = $this->payment_values[$index - 1];
        }//if(Helper::isOdd($size))
        else
        {
            $index = ($size)/2; //middle value
            $first_middle_value = (double)$this->payment_values[$index - 1];
            $second_middle_value = (double)$this->payment_values[$index] ;
            $this->median = ((double)$first_middle_value + (double)$second_middle_value)/2;
        }//end if //if(Helper::isOdd($size))
    }//private function calculateMedian()


    /**
    * @desc Returns Mean Payment
    * @access public
    * @param void
    * @return double
    */
    public function getMean()
    {
        return $this->mean;
    }//public function getMean()


    /**
    * @desc Returns Modal Payment
    * @access public
    * @param void
    * @return double
    */
    public function getMode()
    {
        return $this->mode;
    }//public function getMode()

    /**
    * @desc Returns Median Payment
    * @access public
    * @param void
    * @return double
    */
    public function getMedian()
    {
        return $this->median;
    }//public function getMedian()
    
    
    /**
    * @desc Returns Total Value of Payments
    * @access public
    * @param void
    * @return double
    */
    public function getPaymentValue()
    {
        return $this->payment_total;
    }//public function getPaymentValue()

    /**
    * @desc Returns maximum number of times a payment value occurs
    * @access public
    * @param void
    * @return double
    */
    public function getMaxPaymentOccurence()
    {
        return $this->max_payment_occurence;
    }//public function getMaxPaymentOccurence()
}//class DataAnalyzer 
?>
