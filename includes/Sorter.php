<?php                                 
/**
 * Holds the sort implementation (QuickSort) used
 *
 * @author Benjamin Ugbene
 */
class Sorter
{
    /**
    * @desc Sorts values of array in ascending order: Quick Sort algorithm
    * @access public
    * @param array $sort_array
    * @param string $pivot
    * @return array
    */
    static public function quickSortArrayAsc($sort_array, $pivot = "")
    {
        // Return array as it is empty, or only one element left
        if ( count( $sort_array ) <= 1 )
        {
            return $sort_array;
        }//if ( count( $sort_array ) <= 1 )

        //select pivot value if not supplied
        if(empty($pivot))
        {
            //use first element of array
            $pivot = reset($sort_array);
        }//if(empty($pivot))

        //split array into three partitions
        $same_as = array();
        $greater_than = array();
        $less_than = array();

        foreach ( $sort_array as $value )
        {
            if ( $value < $pivot )
            {
                //value is less than pivot value, so store in less than array
                $less_than[] = $value;
            }//if ( $value < $pivot )
            elseif ( $value == $pivot )
            {
                //value is same as than pivot value,
                //so store in same as than array
                //removes need for recurrsive operation on these elements
                $same_as[] = $value;
            }//elseif ( $value == $pivot )
            else
            {
                //value is greater than pivot value, so store in greater than array
                $greater_than[] = $value;
            }//end if //if ( $value < $pivot )
        }//foreach ( $sort_array as $value )

        //merge sorted partitions
        return array_merge(
                    Sorter::quickSortArrayAsc( $less_than ), //quick sort left partition
                    $same_as,
                    Sorter::quickSortArrayAsc( $greater_than ) //quick sort right partition
                );
    }//static public function quickSortArrayAsc($sort_array, $pivot = "")
}//class Sorter
?>
